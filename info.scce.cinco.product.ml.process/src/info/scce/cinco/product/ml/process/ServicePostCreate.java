package info.scce.cinco.product.ml.process;

import de.jabc.cinco.meta.runtime.hook.CincoPostCreateHook;
import info.scce.cinco.product.ml.process.mlprocess.DataService;
import info.scce.cinco.product.ml.process.mlprocess.EndInputPort;
import info.scce.cinco.product.ml.process.mlprocess.ExternalService;
import info.scce.cinco.product.ml.process.mlprocess.InputPort;
import info.scce.cinco.product.ml.process.mlprocess.InternalService;
import info.scce.cinco.product.ml.process.mlprocess.MLProcess;
import info.scce.cinco.product.ml.process.mlprocess.OutputPort;
import info.scce.cinco.product.ml.process.mlprocess.StartOutputPort;
import jupyter.Function;
import jupyter.Parameter;

public class ServicePostCreate extends CincoPostCreateHook<DataService> {

	
	@Override
	public void postCreate(DataService object) {
		if(object instanceof ExternalService) {
			ExternalService es = (ExternalService)object;
			Function fun = (Function)es.getFun();
			es.setDocumentation(fun.getDocumentation());
			//add inputs
			int y = 0;
			for(Parameter i:fun.getInputs()) {
				InputPort ip = es.newInputPort(i, Definitions.X_OFF, Definitions.Y_OFF+(y*Definitions.PORT_HEIGHT),Definitions.PORT_WIDTH, 18);
				y++;
			}
			//add outputs
			if(fun.getOutput() != null) {
				OutputPort ip = es.newOutputPort(fun.getOutput(), Definitions.X_OFF, Definitions.Y_OFF+(y*Definitions.PORT_HEIGHT),Definitions.PORT_WIDTH, 18);
				y++;
			}
			
		}
		if(object instanceof InternalService) {
			InternalService es = (InternalService)object;
			MLProcess p = es.getProMod();
			es.setDocumentation(p.getDocumentation());
			//add inputs
			int y = 0;
			for(StartOutputPort sop:p.getInputss().get(0).getStartOutputPorts()) {
				//get type
				jupyter.Parameter type = (Parameter) sop.getInputPortSuccessors().get(0).getParameter();
				type.setName(sop.getName());
				InputPort ip = es.newInputPort(type, Definitions.X_OFF, Definitions.Y_OFF+(y*Definitions.PORT_HEIGHT),Definitions.PORT_WIDTH, 18);
				y++;
			}
			//add outputs
			for(EndInputPort eip:p.getOutputss().get(0).getEndInputPorts()) {
				//get type
				jupyter.Parameter type = (Parameter) eip.getOutputPortPredecessors().get(0).getParameter();
				type.setName(eip.getName());
				OutputPort op = es.newOutputPort(type, Definitions.X_OFF, Definitions.Y_OFF+(y*Definitions.PORT_HEIGHT),Definitions.PORT_WIDTH, 18);
				y++;
			}
		}
		int height = Definitions.Y_OFF+((object.getNodes().size())*Definitions.PORT_HEIGHT);
		object.resize(object.getWidth(), height);
		
	}

}
