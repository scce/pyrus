package info.scce.cinco.product.ml.process;
import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProjectService;
import info.scce.pyro.service.rest.Connect_Jupyter_Account;

import javax.json.JsonObject;
import java.util.List;
import java.util.Map;

public class JupyterService extends info.scce.pyro.api.PyroProjectService<PyroProjectService> {

	public boolean canExecute(java.util.List<PyroProjectService> services) {
		return services.isEmpty();
	}

	public boolean isValid(Map<String,String> inputs,List<PyroProjectService> services) {
		if(!inputs.containsKey("Username") || !inputs.containsKey("Token")) {
			return false;
		}
		if(!services.isEmpty()) {
			return false;
		}

		String username = inputs.get("Username");
		String token = inputs.get("Token");

		JupyterUtil util = new JupyterUtil();
		JsonObject userObj = util.checkUser(username,token);

		if(userObj == null) {
			return false;
		}
        return true;
	}
	
	public void execute(PyroProjectService service) {
		//try connection to jupyter service
		
		//Read functions

	}
}
