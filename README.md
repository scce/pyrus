# Installation

* Clone the project to $base directory
* Open a fresh Eclipse Workspace in a Pyro-enabled CINCO
* Import both projects (from root and baseprocess) as Eclipse projects
* Generate the *Base Process* language
    * Right click `info.scce.product.base.process/model/BaseProcessTool.cpd` and select `Generate CINCO Product`
* Generate the Jupyter Functions Ecore Library
    * Open  `info.scce.product.ml.process/JupyterFunctiosn.genmodel`
    * Right click on the first entry in the editor and select `Generate All
`* Generate the *ML Process* language
    * Right click `info.scce.product.ml.process/model/MLProcessTool.cpd` and select `Generate CINCO Product`
* Note: to just generate the *Pyro* code and save time:
    * Right click `info.scce.product.ml.process/model/MLProcessTool.cpd` and select `CINCO Debug Tools > Generate Pyro Product`

# Run Pyro

* Go to `$base/info.scce.product.ml.process/pyro`
* Run `docker-compose up` to start the infrastructure
* Open a second terminal in the same directory
* Run `docker-compose run --rm compiler -dywa-app`
