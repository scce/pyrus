package info.scce.cinco.product.base.process.hook;

import de.jabc.cinco.meta.runtime.hook.CincoPostCreateHook;
import info.scce.cinco.product.base.process.baseprocess.Port;

public class PortPostCreate extends CincoPostCreateHook<Port>{
	
	public static final int Y_OFF = 25;
	public static final int PORT_HIEGHT = 20;

	@Override
	public void postCreate(Port object) {
		int y = (object.getContainer().getNodes().size() -1) * PORT_HIEGHT;
		object.move(5, Y_OFF + y);
		object.getContainer().resize(object.getContainer().getWidth(), Y_OFF + y + PORT_HIEGHT );
		
	}

}
